from rest_framework import *
from rest_framework.serializers import *
from rest_framework.authtoken.models import *
from django.contrib.auth.models import *
from .models import *

def required(value):
    if value is None:
        raise serializers.ValidationError('This field is required')

class FilezipSerializer(Serializer):
    upload = FileField(validators=[required])
