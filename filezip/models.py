from django.db import models
from django.contrib.auth.models import *

class Example(models.Model):
    upload = models.FileField(upload_to='Files/', null=False)

    class Meta:
        db_table = 'example'
