import os
import shutil
from os import path
from django.conf import settings
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework.response import *
from rest_framework.exceptions import PermissionDenied
from .serializers import *
from .models import *
from login.OAuth import *
from datetime import datetime
from zipfile import ZipFile

curr_dir = os.getcwd()
class FilezipC(viewsets.ModelViewSet):
    serializer_class = FilezipSerializer
    queryset = Example.objects.all()

    def list(self, request, *args, **kwargs):
        serializer = FilezipSerializer(self.get_queryset(), many=True)
        print(request.session.get("authorization"))
        if is_authenticated(request):
            return Response({})
        else:
            return Response({"Message": "Please login first"}, status=status.HTTP_401_UNAUTHORIZED)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        file = self.request.FILES.get('upload')
        if is_authenticated(request):
            if serializer.is_valid() :
                shutil.rmtree(curr_dir+'/Files')
                filezipC = Example.objects.create(upload = file)
                file_name = str(filezipC.upload).split("/")[1]
                os.chdir(curr_dir+'/Files')
                ZipFile(file_name+'.zip', mode='w').write(file_name, arcname = file_name)
                zip_file = open(file_name+ ".zip", 'rb')
                response = HttpResponse(zip_file, content_type='application/zip')
                response['Content-Disposition'] = 'attachment; filename='+ file_name + ".zip"
                os.chdir(curr_dir)
                return response
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Message": "Please login first"}, status=status.HTTP_401_UNAUTHORIZED)