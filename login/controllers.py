from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework.response import *
from rest_framework.exceptions import PermissionDenied
from .serializers import *
from .models import *
from .OAuth import *
from datetime import datetime

class LoginController(viewsets.ModelViewSet):
	serializer_class = login_serializer
	queryset = {}

	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid()
		username = serializer.data['username']
		password = serializer.data['password']
		token = get_token(username, password)
		if token == None :
			return Response({"Message": "The username or password is incorrect"}, status=status.HTTP_401_UNAUTHORIZED)
		request.session['authorization'] = 'Bearer ' + token
		return Response({"Message": "Feel free to use our API."}, status=status.HTTP_202_ACCEPTED)
